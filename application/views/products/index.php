<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
</head>
<body>

<h1>Product List</h1>


<table border="1">
    <tr>
        <th>ID</th>
        <th>Nama Produk</th>
        <th>Harga</th>
        <th>Kategori ID</th>
        <th>Status ID</th>
        <th>Action</th>
    </tr>
    <?php foreach ($products as $product): ?>
        <tr>
            <td><?= $product['id_produk']; ?></td>
            <td><?= $product['nama_produk']; ?></td>
            <td><?= $product['harga']; ?></td>
            <td><?= $product['kategori_id']; ?></td>
            <td><?= $product['status_id']; ?></td>
            <td>
                <a href="<?= base_url('products/edit/' . $product['id_produk']); ?>">Edit</a> |
                <a href="<?= base_url('products/delete/' . $product['id_produk']); ?>" onclick="return confirm('Are you sure you want to delete this product?')">Delete</a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>


<h2>Add Product</h2>
<?= form_open('products/add'); ?>
    <label for="nama_produk">Nama Produk:</label>
    <input type="text" name="nama_produk" required>
    <br>
    <label for="harga">Harga:</label>
    <input type="number" name="harga" required>
    <br>
    <label for="kategori_id">Kategori ID:</label>
    <input type="text" name="kategori_id">
    <br>
    <label for="status_id">Status ID:</label>
    <input type="text" name="status_id">
    <br>
    <input type="submit" value="Add Product">
<?= form_close(); ?>

</body>
</html>
