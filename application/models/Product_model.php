<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

    public function getSellableProducts() {
        
        return $this->db->select('*')
            ->from('Produk')
            ->where('status_id', 1)
            ->get()
            ->result_array();
    }

    public function addProduct() {        
        $data = array(
            'nama_produk' => $this->input->post('nama_produk'),
            'harga' => $this->input->post('harga'),            
            'kategori_id' => $this->input->post('kategori_id'),
            'status_id' => $this->input->post('status_id'),
        );

        $this->db->insert('Produk', $data);
    }

    public function editProduct($id) {        
        $data = array(
            'nama_produk' => $this->input->post('nama_produk'),
            'harga' => $this->input->post('harga'),            
            'kategori_id' => $this->input->post('kategori_id'),
            'status_id' => $this->input->post('status_id'),
        );

        $this->db->where('id_produk', $id);
        $this->db->update('Produk', $data);
    }

    public function getProductById($id) {        
        return $this->db->where('id_produk', $id)
            ->get('Produk')
            ->row_array();
    }

    public function deleteProduct($id) {        
        $this->db->where('id_produk', $id);
        $this->db->delete('Produk');
    }

    public function saveProductsFromApi($api_data) {        
        foreach ($api_data as $product) {
            $data = array(
                'nama_produk' => $product['nama_produk'],
                'harga' => $product['harga'],
                'kategori_id' => $product['kategori_id'],
                'status_id' => $product['status_id'],
            );

            $this->db->insert('Produk', $data);
        }
    }
}
