<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    public function index() {    
        $this->load->model('Product_model');
    
        $data['products'] = $this->Product_model->getSellableProducts();
    
        $this->load->view('products/index', $data);
    }

    public function add() {    
        $this->load->library('form_validation');
        $this->load->model('Product_model');
    
        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required|numeric');

        if ($this->form_validation->run() === FALSE) {        
            $this->load->view('products/add');
        } else {        
            $this->Product_model->addProduct();
        
            redirect('products');
        }
    }

    public function edit($id) {    
        $this->load->library('form_validation');
        $this->load->model('Product_model');
    
        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required|numeric');

        if ($this->form_validation->run() === FALSE) {        
            $data['product'] = $this->Product_model->getProductById($id);
            $this->load->view('products/edit', $data);
        } else {        
            $this->Product_model->editProduct($id);
        
            redirect('products');
        }
    }

    public function delete($id) {    
        $this->load->model('Product_model');
    
        $product = $this->Product_model->getProductById($id);
        if ($product) {        
            $this->Product_model->deleteProduct($id);
        
            redirect('products');
        } else {        
            redirect('products');
        }
    }

    public function syncWithApi() {
		$this->load->model('Product_model');
		$this->load->library('curl');
		
		$username = 'tesprogrammer' . date('dm') . 'C10';
		
		$password = md5('bisacoding-' . date('d-m-y'));
		
		$api_url = 'https://recruitment.fastprint.co.id/tes/api_tes_programmer';
		
		$post_data = array(		
		);
		
		$curl_options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post_data,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded',
			),
			CURLOPT_USERPWD => $username . ':' . $password,
		);
		
		$this->curl->create($api_url);
		
		$this->curl->options($curl_options);
		
		$api_response = $this->curl->execute();
		
		$api_data = json_decode($api_response, true);
		
		$this->Product_model->saveProductsFromApi($api_data);
		
		$data['products'] = $this->Product_model->getSellableProducts();
		
		$this->load->view('products/index', $data);
	}
}
