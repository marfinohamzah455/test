# Aplikasi Manajemen Produk

Ini adalah aplikasi web sederhana berbasis CodeIgniter untuk mengelola produk. Ini memungkinkan pengguna untuk menambah, mengedit, dan menghapus produk, serta menyinkronkan data dengan API eksternal.

## Fitur

- Lihat daftar produk dengan kemampuan untuk mengedit dan menghapus setiap produk.
- Tambahkan produk baru menggunakan formulir dengan validasi.
- Edit produk yang ada menggunakan formulir dengan validasi.
- Hapus produk dengan peringatan konfirmasi.
- Sinkronisasi dengan API eksternal untuk mengambil dan menyimpan produk.

## Persyaratan

-PHP >= 5.6
- CodeIgniter 3.x
- Server web (mis., Apache, Nginx)
- Basis data MySQL atau PostgreSQL

## Instalasi

1. Kloning repositori:

     ``` pesta
     git clone https://github.com/nama-pengguna-anda/product-management-app.git
     ```

2. Konfigurasikan server web Anda agar mengarah ke direktori `publik`.

3. Buat database dan konfigurasikan pengaturan database di `application/config/database.php`.

4. Jalankan migrasi database untuk menyiapkan tabel yang diperlukan:

     ``` pesta
     indeks php.php bermigrasi
     ```

5. Buka aplikasi di browser web Anda.

## Penggunaan

1. Navigasikan ke daftar produk:

     ```teks
     http://domainanda.com/index.php/products
     ```

2. Gunakan tautan yang disediakan untuk menambah, mengedit, dan menghapus produk.

3. Untuk melakukan sinkronisasi dengan API:

     ```teks
     http://domainanda.com/index.php/products/sync
     ```

## Konfigurasi

- Sesuaikan rute di `application/config/routes.php` sesuai kebutuhan.
- Ubah pengaturan database di `application/config/database.php`.

## Penyelesaian masalah

- Jika Anda mengalami masalah apa pun, periksa log CodeIgniter di `application/logs`.
- Pastikan server web Anda memiliki izin yang diperlukan untuk membaca dan menulis.

## Berkontribusi

Jangan ragu untuk berkontribusi pada proyek ini dengan membuka terbitan atau mengirimkan permintaan penarikan.

## Lisensi

Proyek ini dilisensikan di bawah [Lisensi MIT](LISENSI).
