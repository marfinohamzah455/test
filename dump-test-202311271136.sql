PGDMP         $            
    {            test    14.3    14.3                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    223740    test    DATABASE     O   CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'C';
    DROP DATABASE test;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1259    223749    kategori    TABLE     m   CREATE TABLE public.kategori (
    id_kategori integer NOT NULL,
    nama_kategori character varying(255)
);
    DROP TABLE public.kategori;
       public         heap    postgres    false    3            �            1259    223748    kategori_id_kategori_seq    SEQUENCE     �   CREATE SEQUENCE public.kategori_id_kategori_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.kategori_id_kategori_seq;
       public          postgres    false    212    3                       0    0    kategori_id_kategori_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.kategori_id_kategori_seq OWNED BY public.kategori.id_kategori;
          public          postgres    false    211            �            1259    223742    produk    TABLE     �   CREATE TABLE public.produk (
    id_produk integer NOT NULL,
    nama_produk character varying(255),
    harga numeric(10,2),
    kategori_id integer,
    status_id integer
);
    DROP TABLE public.produk;
       public         heap    postgres    false    3            �            1259    223741    produk_id_produk_seq    SEQUENCE     �   CREATE SEQUENCE public.produk_id_produk_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.produk_id_produk_seq;
       public          postgres    false    210    3                       0    0    produk_id_produk_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.produk_id_produk_seq OWNED BY public.produk.id_produk;
          public          postgres    false    209            �            1259    223756    status    TABLE     g   CREATE TABLE public.status (
    id_status integer NOT NULL,
    nama_status character varying(255)
);
    DROP TABLE public.status;
       public         heap    postgres    false    3            �            1259    223755    status_id_status_seq    SEQUENCE     �   CREATE SEQUENCE public.status_id_status_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.status_id_status_seq;
       public          postgres    false    214    3                       0    0    status_id_status_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.status_id_status_seq OWNED BY public.status.id_status;
          public          postgres    false    213            q           2604    223752    kategori id_kategori    DEFAULT     |   ALTER TABLE ONLY public.kategori ALTER COLUMN id_kategori SET DEFAULT nextval('public.kategori_id_kategori_seq'::regclass);
 C   ALTER TABLE public.kategori ALTER COLUMN id_kategori DROP DEFAULT;
       public          postgres    false    212    211    212            p           2604    223745    produk id_produk    DEFAULT     t   ALTER TABLE ONLY public.produk ALTER COLUMN id_produk SET DEFAULT nextval('public.produk_id_produk_seq'::regclass);
 ?   ALTER TABLE public.produk ALTER COLUMN id_produk DROP DEFAULT;
       public          postgres    false    210    209    210            r           2604    223759    status id_status    DEFAULT     t   ALTER TABLE ONLY public.status ALTER COLUMN id_status SET DEFAULT nextval('public.status_id_status_seq'::regclass);
 ?   ALTER TABLE public.status ALTER COLUMN id_status DROP DEFAULT;
       public          postgres    false    213    214    214                      0    223749    kategori 
   TABLE DATA                 public          postgres    false    212   �                 0    223742    produk 
   TABLE DATA                 public          postgres    false    210   �       	          0    223756    status 
   TABLE DATA                 public          postgres    false    214   �                  0    0    kategori_id_kategori_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.kategori_id_kategori_seq', 1, false);
          public          postgres    false    211                       0    0    produk_id_produk_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.produk_id_produk_seq', 1, false);
          public          postgres    false    209                       0    0    status_id_status_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.status_id_status_seq', 1, false);
          public          postgres    false    213            v           2606    223754    kategori kategori_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (id_kategori);
 @   ALTER TABLE ONLY public.kategori DROP CONSTRAINT kategori_pkey;
       public            postgres    false    212            t           2606    223747    produk produk_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.produk
    ADD CONSTRAINT produk_pkey PRIMARY KEY (id_produk);
 <   ALTER TABLE ONLY public.produk DROP CONSTRAINT produk_pkey;
       public            postgres    false    210            x           2606    223761    status status_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id_status);
 <   ALTER TABLE ONLY public.status DROP CONSTRAINT status_pkey;
       public            postgres    false    214               
   x���             
   x���          	   
   x���         